# Google AppEngine Server Bundle

This bundle helps to create worker services for custom php runtime environment in Google App Engine.

## Create worker project

    composer require symfony/flex --dev
    composer require symfony/dotenv --dev

    composer require symfony/framework-bundle
    composer require symfony/yaml
    composer require symfony/monolog-bundle
    composer require softspring/gae-worker-server

**config/dev/monolog.yaml**
    
    monolog:
        handlers:
            main:
                type: stream
                path: "php://stdout"
                level: debug

**config/prod/monolog.yaml**
    
    monolog:
        handlers:
            error_log:
                type: error_log
                level: info

**Dockerfile**

    # The Google App Engine php runtime is Debian Jessie with PHP installed
    # and various os-level packages to allow installation of popular PHP
    # libraries. The source is on github at:
    #   https://github.com/GoogleCloudPlatform/php-docker
    FROM gcr.io/google_appengine/php
    
    # Add our php.ini config
    ENV PHP_INI_OVERRIDE=php-worker.ini
    
    # override the default CMD for the pubsub worker
    CMD php bin/worker-entrypoint.php

**php-worker.ini**

    suhosin.executor.func.blacklist=

## Configure server bundle
    
### Configure config/gae_worker_server.yaml

    gae_worker_server:
        job: 'app.job_processor'
        gcloud_project_id: '%env(GOOGLE_CLOUD_PROJECT)%'
        pubsub_topic: '<topic-name>'
        pubsub_suscription: '<subscription-name>'
        pubsub_max_messages: 1
        address: '0.0.0.0'
        port: 8080

### Add to .env and .env.dist

    ###> AppEngine environment variables ###
    # see https://cloud.google.com/appengine/docs/flexible/php/runtime#environment_variables
    GOOGLE_CLOUD_PROJECT=<project-id>
    # see https://cloud.google.com/docs/authentication/getting-started
    GOOGLE_APPLICATION_CREDENTIALS=<local-auth-json-file-location>
    ###< AppEngine environment variables ###
    
### Create your JobProcessor

    # config/services.yaml
    app.job_processor:
        class: App\JobProcessor\MyJobProcessor

    # src/JobProcessor/MyJobProcessor.php
    <?php
    
    namespace App\JobProcessor;
    
    use Google\Cloud\PubSub\Message;
    use Softspring\GaeWorkerServerBundle\Server\JobProcessorInterface;
    
    class MyJobProcessor implements JobProcessorInterface
    {
        public function work(Message $message): bool
        {
            return true;
        }
    }

### Create bin/worker-entrypoint.php

    <?php
    
    use App\Kernel;
    use Symfony\Component\Debug\Debug;
    use Symfony\Component\Dotenv\Dotenv;
    
    set_time_limit(0);
    
    require __DIR__.'/../vendor/autoload.php';
    
    if (!isset($_SERVER['APP_ENV'])) {
        (new Dotenv())->load(__DIR__.'/../.env');
    }
    
    $env = $_SERVER['APP_ENV'];
    $debug = $_SERVER['APP_DEBUG'];
    
    if ($debug) {
        umask(0000);
    
        if (class_exists(Debug::class)) {
            Debug::enable();
        }
    }
    
    $kernel = new Kernel($env, $debug);
    $kernel->boot();
    $kernel->getContainer()->get('logger')->info('Starting worker entrypoint');
    $server = $kernel->getContainer()->get('gae_worker_server');
    $server->run();