<?php

namespace Softspring\GaeWorkerServerBundle\Server;

use Google\Cloud\PubSub\PubSubClient;
use Google\Cloud\PubSub\Subscription;
use Psr\Log\LoggerInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\Server\IoServer;

class ServerFactory
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var PubSubClient
     */
    protected $pubSubClient;

    /**
     * @var JobProcessorInterface
     */
    protected $jobProcessor;

    /**
     * ServerFactory constructor.
     *
     * @param LoggerInterface $logger
     * @param PubSubClient    $pubSubClient
     */
    public function __construct(LoggerInterface $logger, PubSubClient $pubSubClient, JobProcessorInterface $jobProcessor)
    {
        $this->logger = $logger;
        $this->pubSubClient = $pubSubClient;
        $this->jobProcessor = $jobProcessor;
    }

    public function createServer(MessageComponentInterface $component, $topicName, $subscriptionName, $pubSubMaxMessages = 1000, $port = 80, $address = '0.0.0.0')
    {
        $server = IoServer::factory($component, $port, $address);

        // create the job and worker
        $worker = new PubSubWorker($this->getSubscription($topicName, $subscriptionName), $this->jobProcessor, $this->logger, $pubSubMaxMessages);

        // add our worker to the event loop
        $server->loop->addPeriodicTimer(0, $worker);

        $this->logger->info('PubSub worker server is running');

        return $server;
    }

    /**
     * @param string $topicName
     * @param string $subscriptionName
     *
     * @return Subscription
     */
    protected function getSubscription(string $topicName, string $subscriptionName)
    {
        $this->logger->info(sprintf('Listening PubSub topic "%s" at suscription "%s"', $topicName, $subscriptionName));
        $topic = $this->pubSubClient->topic($topicName);

        $subscription = $topic->subscription($subscriptionName);

        if (!$subscription->exists()) {
            $subscription->create();
        }

        return $subscription;
    }
}