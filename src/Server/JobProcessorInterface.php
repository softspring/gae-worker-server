<?php

namespace Softspring\GaeWorkerServerBundle\Server;

use Google\Cloud\PubSub\Message;

interface JobProcessorInterface
{
    /**
     * @param Message $message
     *
     * @return bool
     */
    public function work(Message $message): bool;
}