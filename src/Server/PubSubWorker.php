<?php

namespace Softspring\GaeWorkerServerBundle\Server;

use Google\Cloud\PubSub\Subscription;
use Google\Cloud\PubSub\Message;
use GuzzleHttp\Promise\Promise;
use Psr\Log\LoggerInterface;

class PubSubWorker
{
    private $callback;
    private $connection;

    /**
     * @var Promise
     */
    private $promise;
    private $subscription;

    private $pubSubMaxMessages;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Subscription $subscription, JobProcessorInterface $job, LoggerInterface $logger, $pubSubMaxMessages = 1000)
    {
        $this->logger = $logger;

        // [START callback]
        $callback = function ($response) use ($job, $subscription, $logger) {
            $ackMessages = [];
            $messages = json_decode($response->getBody(), true);
            if (isset($messages['receivedMessages'])) {
                foreach ($messages['receivedMessages'] as $message) {
                    $pubSubMessage = new Message($message['message'], array('ackId' => $message['ackId']));

                    $logger->info(sprintf('Processing message "%s" published on %s', $message['message']['messageId'], $message['message']['publishTime']));

                    if ($job->work($pubSubMessage)) {
                        $ackMessages[] = $pubSubMessage;

                        $logger->info(sprintf('Acknowledging message "%s"', $message['message']['messageId']));
                    }
                }
            }
            // Acknowledge the messsages have been handled
            if (!empty($ackMessages)) {
                $subscription->acknowledgeBatch($ackMessages);
            }
        };
        // [END callback]
        $this->callback = $callback;
        $this->subscription = $subscription;
        $this->connection = new AsyncConnection();
        $this->pubSubMaxMessages = $pubSubMaxMessages;
    }

    public function __invoke($timer)
    {
        // advance the event loop for our async call to pubsub
        $this->connection->tick();
        // check the status of the promise and handle completion or error
        if (!$this->promise || 'fulfilled' == $state = $this->promise->getState()) {
            $this->asyncPubsubPull();
        } elseif ('rejected' == $state) {
            // this will throw the exception and stop the event loop
            $this->promise->wait();
            $this->logger->debug('Rejected promise');
        }
    }

    private function asyncPubsubPull()
    {
        $callback = $this->callback;
        // [START promise]
        $promise = $this->connection->pull([
            'maxMessages' => $this->pubSubMaxMessages,
            'subscription' => $this->subscription->info()['name'],
        ]);
        $promise->then($callback);
        // [END promise]
        $this->promise = $promise;
    }
}