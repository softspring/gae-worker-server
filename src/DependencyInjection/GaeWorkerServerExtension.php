<?php

namespace Softspring\GaeWorkerServerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class GaeWorkerServerExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('gae_worker_server.gcloud_project_id', $config['gcloud_project_id']);
        $container->setParameter('gae_worker_server.pubsub_topic', $config['pubsub_topic']);
        $container->setParameter('gae_worker_server.pubsub_suscription', $config['pubsub_suscription']);
        $container->setParameter('gae_worker_server.pubsub_max_messages', $config['pubsub_max_messages']);
        $container->setParameter('gae_worker_server.address', $config['address']);
        $container->setParameter('gae_worker_server.port', $config['port']);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yaml');

        $serverFactoryDefinition = $container->getDefinition('gae_worker_server.server_factory');
        $serverFactoryDefinition->setArgument(2, new Reference($config['job']));
    }
}