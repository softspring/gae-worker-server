<?php

namespace Softspring\GaeWorkerServerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('gae_worker_server');

        $rootNode
            ->children()
                ->scalarNode('job')->isRequired()->end()
                ->scalarNode('gcloud_project_id')->isRequired()->end()
                ->scalarNode('pubsub_topic')->isRequired()->end()
                ->scalarNode('pubsub_suscription')->isRequired()->end()
                ->integerNode('pubsub_max_messages')->defaultValue(1000)->end()
                ->scalarNode('address')->defaultValue('0.0.0.0')->end()
                ->integerNode('port')->defaultValue(8080)->end()
            ->end()
        ;

        return $treeBuilder;
    }
}